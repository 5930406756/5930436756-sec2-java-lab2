package kamkrue.pisupa.lab2;
// Integerssorter is a program about sorting the numbers.
// Mr.Pisupa Kamkrue No.593040675-6 Sec. 2
import java.util.Arrays;

public class IntegersSorter {

	public static void main(String[] args) {

		int number = Integer.parseInt(args[0]);

		if (number > 1) {
			int[] sorter = new int[number];

			for (int n = 0; n < sorter.length; n++) {
				sorter[n] = Integer.parseInt(args[1 + n]);
			}

			System.out.println("Before sorting:" + Arrays.toString(sorter));
			Arrays.sort(sorter);
			System.out.println("After sorting:" + Arrays.toString(sorter));

		}

		else {
			System.err.println("IntegersSorter <the number of integers to sort> <i1> <i2> ...<in>");
			System.exit(0);
		}
	}

}
