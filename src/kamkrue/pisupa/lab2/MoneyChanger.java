package kamkrue.pisupa.lab2;
// Moneychanger is a program about exchanging the money.  
// Mr.Pisupa Kamkrue No.593040675-6 Sec. 2
public class MoneyChanger {

	public static void main(String[] args) {
		System.out.println("You give 1000 for the cost as " + args[0] + " Baht.");
		int exch = (1000 - Integer.parseInt(args[0])); // exch is exchange. 
		int bankNote500 = (exch / 500);
		int bankNote100 = ((exch % 500) / 100);
		int bankNote50 = (((exch % 500) % 100) / 50);
		int bankNote20 = ((((exch % 500) % 100) % 50) / 20);
		if (exch !=0 ){
			System.out.println("You will receive exchange as " + exch + " Baht."); 
				if( bankNote20==0 && bankNote50!=0 && bankNote500!=0 && bankNote100!=0 ){
					System.out.println( bankNote500 + " of 500 bank note; " + bankNote100 + " of 100 bank note; " + bankNote50 + " of 50 bank note;" );
				}
				else if ( bankNote20==0 && bankNote50!=0 && bankNote500!=0 && bankNote100==0 ){
					System.out.println( bankNote500 + " of 500 bank note; "  + bankNote50 + " of 50 bank note;" );
				}
				else if ( bankNote20!=0 && bankNote100!=0 && bankNote500!=0 && bankNote50!=0 ) {
					System.out.println( bankNote500 +" of 500 bank note; " + bankNote100 +" of 100 bank note; " + bankNote50 +" of 50 bank note; "+ bankNote20 +" of 20 bank note;" );
				}
				else if ( bankNote20!=0 && bankNote100!=0 && bankNote500!=0 && bankNote50==0 ) {
					System.out.println( bankNote500 +" of 500 bank note; " + bankNote100 +" of 100 bank note; " + bankNote20 +" of 20 bank note;" );
				}
				else if ( bankNote500==0 && bankNote100!=0 && bankNote50==0  && bankNote20==0 ){
					System.out.println( bankNote100 + " of 100 bank note; ") ; 
				}
				else if ( bankNote500!=0 && bankNote100!=0 && bankNote50==0 && bankNote20==0 ) {
					System.out.println( bankNote500 +" of 500 bank note "+ bankNote100 +" of 100 bank note" );
				}
				else if ( bankNote20!=0 && bankNote100==0 && bankNote50!=0 && bankNote500!=0 ){
					System.out.println( bankNote500 +" of 500 bank note; " + bankNote50 +" of 50 bank note "+ bankNote20 +" of 20 bank note" );
				}
				else if ( bankNote500==0 && bankNote100!=0 && bankNote50!=0 && bankNote20!=0 ){
					System.out.println( bankNote100 + " of 100 bank note; "+bankNote50 +" of 50 bank note "+ bankNote20 +" of 20 bank note" ) ;				
				}
				else if ( bankNote500==0 && bankNote100!=0 && bankNote50!=0 && bankNote20==0 ){
					System.out.println( bankNote100 + " of 100 bank note;  "+bankNote50 +" of 50 bank note;" ) ;			
				}
				else if ( bankNote500==0 && bankNote100!=0 && bankNote50==0 && bankNote20!=0 ){
					System.out.println( bankNote100 +" of 100 bank note; " + bankNote20 +" of 20 bank note;" ) ;		
				}
				else if ( bankNote100==0 && bankNote50==0 && bankNote20==0 && bankNote500!=0 ){
					System.out.println( bankNote500 +" of 500 bank note; " );
				}
				else if ( bankNote50==0 && bankNote20!=0 && bankNote500!=0 && bankNote100==0 ){
					System.out.println( bankNote500 + " of 500 bank note; " + bankNote20 + " of 20 bank note;" );
				}
				else if ( bankNote20!=0 && bankNote500==0 && bankNote100==0 && bankNote50==0 ){
					System.out.println( bankNote20 + " of 20 bank note; " );
				}
				else if ( bankNote50!=0 && bankNote500==0 && bankNote100==0 && bankNote20==0 ){
					System.out.println( bankNote50 + " of 50 bank note; " );
				}
				else if ( bankNote20!=0 && bankNote100==0 && bankNote50!=0 && bankNote500==0 ){
					System.out.println( bankNote50 + " of 50 bank note; " + bankNote20 + " of 20 bank note;" );
				}
		}	 
		else {
			System.out.println("No change");
			System.exit(0);

		}
	}
}
