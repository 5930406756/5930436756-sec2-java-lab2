package kamkrue.pisupa.lab2;
// PatientV2 is a program about patient information.
// Mr.Pisupa Kamkrue No.593040675-6 Sec. 2
public class PatientV2 {

	public static void main(String[] args) {
		String male = "Male";
		String female = "Female";
		String gender = args[1];

		if (args.length < 4) {
			System.err.println("Patient <name> <gender> <weight> <height>");
			System.exit(0);
		}

		float weight = Float.parseFloat(args[2]); 
		int height = Integer.parseInt(args[3]);
		if (args.length == 4) {
			System.out.println("This patient name is " + args[0]);

			if (gender.equalsIgnoreCase(female)) {

				if (weight > 0) {

					if (height > 0) {
						System.out.println("Her weight is " + weight + " kg." + " and height is " + height + " cm.");
					}

					else {
						System.out.println("Height must be non-negative");
					}
				}

				else {
					System.out.println("Weight must be non-negative");
				}
			}

			else if (gender.equalsIgnoreCase(male)) {

				if (weight > 0) {

					if (height > 0) {
						System.out.println("His weight is " + weight + " kg." + " and height is " + height + " cm.");
					}

					else {
						System.out.println("Height must be non-negative");
					}
				}

				else {
					System.out.println("Weight must be non-negative");
				}
			}

			else {
				System.out.println("Please enter gender as only Male of Female");
			}
		}

		else {
			System.err.println("Paient <name> <gender> <weight> <height>");
		}
	}

}

